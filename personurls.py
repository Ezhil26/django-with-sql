from rest_framework import routers
from .views import PersonAPI
from django.urls import path,include
router=routers.DefaultRouter()
router.register('person',PersonAPI,basename='person')


urlpatterns=[
    path('person/',include(router.urls)),

]